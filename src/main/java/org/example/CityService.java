package org.example;

import org.example.model.City;

import java.util.List;

public interface CityService {
    List<City> getAllCities();

    City getCityById(Long id);

    List<City> updateCityList(City city);
}
