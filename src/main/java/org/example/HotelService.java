package org.example;

import org.example.model.City;
import org.example.model.Hotel;

import java.util.List;
import java.util.Map;

public interface HotelService {
        /*
    In order to test , you can initialize your full list of Hotels in the constructor;

    public HotelService(List<Hotel> hotels){
    }
*/
    List<Hotel> getHotelsByCity(Long cityId);

    Hotel getHighestRatedHotelForCity(Long cityId);

    Map<City, List<Hotel>> groupHotelsByCity(Long cityId);

    Hotel getClosestHotelToCity(Long cityId);

    List<Hotel> updateHotelList(Hotel hotel);
}
