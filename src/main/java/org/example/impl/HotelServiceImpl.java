package org.example.impl;

import org.example.HotelService;
import org.example.model.City;
import org.example.model.HaversineFormula;
import org.example.model.Hotel;

import java.util.*;
import java.util.stream.Collectors;

public class HotelServiceImpl implements HotelService {
    private List<Hotel> hotelsList;
    private Hotel hotel;
    private City city;

    //can you pass in more than 1 thing to constructor i.e. Cities?
    public HotelServiceImpl(List<Hotel> hotels){
        this.hotelsList = hotels;
//        this.city = city;
    }

    @Override
    public List<Hotel> getHotelsByCity(Long cityId) {
        return hotelsList.stream()
                .filter(h -> cityId.equals(h.getCity().getCityId()))
                .collect(Collectors.toList());
    }

    @Override
    public Hotel getHighestRatedHotelForCity(Long cityId) {
        return hotelsList.stream()
                .filter(h -> cityId.equals(h.getCity().getCityId()))
                .max(Comparator.comparing(Hotel :: getRating)).get();
    }

    @Override
    public Map<City, List<Hotel>> groupHotelsByCity(Long cityId) {
        Map<City, List<Hotel>> cityHotelsMap = new HashMap<>();
        List<Hotel> hotelsByCityList = getHotelsByCity(cityId);

        cityHotelsMap.put(hotelsByCityList.get(0).getCity(), hotelsByCityList);
        return cityHotelsMap;
    }

    @Override
    public Hotel getClosestHotelToCity(Long cityId) {
        List<Hotel> hotelsByCityList = getHotelsByCity(cityId);
        return hotelsByCityList.stream()
                .min(Comparator.comparing(h -> HaversineFormula.haversine(h.getLongitude(), h.getLatitude(), h.getCity().getCityCentreLongitude(), h.getCity().getCityCentreLatitude())))
                .orElseThrow(null);
    }

    @Override
    public List<Hotel> updateHotelList(Hotel hotel) {
        if (hotelsList.contains(hotel)) {}
        else {
            hotelsList.add(hotel);
        }
        return null;
    }
}
