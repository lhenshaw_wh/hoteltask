package org.example.model;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter@Setter

public class City {
    private final long cityId;
    private final String cityName;
    private double cityCentreLatitude;
    private double cityCentreLongitude;

    public City(long cityId, String CityName, double cityCentreLatitude, double cityCentreLongitude) {
        this.cityId = cityId;
        cityName = CityName;
        this.cityCentreLatitude = cityCentreLatitude;
        this.cityCentreLongitude = cityCentreLongitude;
    }
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof City)) {
            return false;
        }
        // If the object is compared with itself then return true
        City city = (City) o;
//        boolean cityBoolean = (this.cityId == null && city.cityId == null)
//                || (this.currencyCode != null && this.currencyCode.equals(other.currencyCode));
        return this.cityId == city.cityId && Objects.equals(this.cityName, city.cityName) && this.cityCentreLatitude == city.cityCentreLatitude && this.cityCentreLongitude == city.cityCentreLongitude;
    }
    @Override
    public int hashCode() {
        return 5 * (Long.valueOf(this.cityId)).hashCode() * this.cityName.hashCode() * new Double(this.cityCentreLatitude).hashCode() * new Double(this.cityCentreLongitude).hashCode();
    }
}
