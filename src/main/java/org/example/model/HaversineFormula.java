package org.example.model;

import static java.lang.Math.*;

public class HaversineFormula {

    public static double haversine(double lon1, double lat1, double lon2, double lat2){
        double latDistance = toRadians(lat1 - lat2);
        double lonDistance = toRadians(lon1 - lon2);
        double a = pow(sin(latDistance / 2), 2) + (cos(toRadians(lat1)) * cos(toRadians(lat2)) * pow(sin(lonDistance / 2), 2));
        double c = 2 * atan2(sqrt(a), sqrt(1-a));
        return c;
    }
}
