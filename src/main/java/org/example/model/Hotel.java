package org.example.model;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter@Setter
public class Hotel {
    private final long hotelId;
    private final String hotel_name;
    private final City city;
    private final double latitude;
    private final double longitude;
    private final int rating;

    public Hotel(long l, String hotel_name, City city, double longitude, double latitude, int rating) {
        this.hotelId = l;
        this.hotel_name = hotel_name;
        this.city = city;
        this.longitude = longitude;
        this.latitude = latitude;
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Hotel)) {
            return false;
        }
        Hotel hotel = (Hotel) o;
        return this.hotelId == hotel.hotelId && Objects.equals(this.hotel_name, hotel.hotel_name) && this.city.equals(hotel.city) && this.longitude == hotel.longitude && this.latitude == hotel.latitude && this.rating == hotel.rating;
    }
    @Override
    public int hashCode() {
        return 5 * (Long.valueOf(this.hotelId)).hashCode() * this.hotel_name.hashCode() * this.city.hashCode() * (Integer.valueOf(this.rating)).hashCode() * new Double(this.longitude).hashCode();
    }
}
