package org.example;

import org.example.impl.HotelServiceImpl;
import org.example.model.City;
import org.example.model.Hotel;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class HotelServiceTest {

    private static HotelServiceImpl hotelServiceImpl;

    @Test
    public void Should_showListOfHotels_whenCityIdCalled() {
        hotelServiceImpl = setUpData();
        List<Hotel> expectedLondonHotels = new ArrayList<>();
        City city = setupExpectedCityData(1, "London", 51.5072, 0.1276);
        expectedLondonHotels.add(setupExpectedHotelData(1, "Premier Inn", city, 51.5075, 0.1278, 2));
        expectedLondonHotels.add(setupExpectedHotelData(2, "Arms Inn", city, 51.5064, 0.1244, 3));
        expectedLondonHotels.add(setupExpectedHotelData(3, "Edison Hotel", city, 51.5101, 0.1231, 5));
       /* expectedLondonHotels.add(hotels.get(0));
        // you dont want to do this, because you are adding a reference to a list to your expectedLondonHotels and if the reference changes so will your expected list
        */
        List<Hotel> actualHotels =  hotelServiceImpl.getHotelsByCity(1L);
        assertEquals(expectedLondonHotels, actualHotels); // this will fail because you will have to implement the equals() and hashcode() methods in the Hotel class
//        assertEquals(expectedLondonHotels, actualHotels);
    }

    private Hotel setupExpectedHotelData(long id, String hotelName, City city, double longitude, double latitude, int rating ){
        return new Hotel(id,hotelName,city,longitude,latitude,rating);
    }

    private City setupExpectedCityData(long id, String cityName, double longitude, double latitude ){
        return new City(id,cityName,longitude,latitude);
    }

    @Test
    public void testCityEquals(){
        City city = setupExpectedCityData(1, "London", 51.5072, 0.1276);
        City city1 = setupExpectedCityData(1, "London", 51.5072, 0.1276);

        assertEquals(city, city1);
    }


    @Test
    public void Should_getHighestRatedHotelForCity_WhenGivenCityId() {
        hotelServiceImpl = setUpData();
        List<Hotel> expectedLondonHotels = new ArrayList<>();
        City city = setupExpectedCityData(1, "London", 51.5072, 0.1276);
        expectedLondonHotels.add(setupExpectedHotelData(1, "Premier Inn", city, 51.5075, 0.1278, 2));
        expectedLondonHotels.add(setupExpectedHotelData(2, "Arms Inn", city, 51.5064, 0.1244, 3));
        expectedLondonHotels.add(setupExpectedHotelData(3, "Edison Hotel", city, 51.5101, 0.1231, 5));

        Hotel expected = expectedLondonHotels.get(2);
        Hotel actual = hotelServiceImpl.getHighestRatedHotelForCity(1L);
        System.out.println("actual = " + actual);
//        assertEquals(expected, hotelServiceImpl.getHighestRatedHotelForCity(1L));

        assertEquals(expected, actual);
    }

    @Test
    public void Should_groupHotelsByCity_WhenGivenCityId() {
        hotelServiceImpl = setUpData();
        List<Hotel> expectedLondonHotels = new ArrayList<>();
        City city = setupExpectedCityData(1, "London", 51.5072, 0.1276);
        expectedLondonHotels.add(setupExpectedHotelData(1, "Premier Inn", city, 51.5075, 0.1278, 2));
        expectedLondonHotels.add(setupExpectedHotelData(2, "Arms Inn", city, 51.5064, 0.1244, 3));
        expectedLondonHotels.add(setupExpectedHotelData(3, "Edison Hotel", city, 51.5101, 0.1231, 5));

        Map<City, List<Hotel>> cityHotelsMap = new HashMap<>();
        cityHotelsMap.put(city, expectedLondonHotels);

        Map<City, List<Hotel>> expected = cityHotelsMap;
        Map<City, List<Hotel>> actual = hotelServiceImpl.groupHotelsByCity(1L);

        assertEquals(expected, actual);
    }

    @Test
    public void Should_getClosestHotelToCity_WhenGivenCityId() {
        hotelServiceImpl = setUpData();
        City city = new City(5, "Cardiff", 51.4837, 3.1681);

        Hotel expected = setupExpectedHotelData(14, "Welsh Inn", city, 51.4512, 3.1581, 3);
        Hotel actual = hotelServiceImpl.getClosestHotelToCity(5L);

        assertEquals(expected, actual);
    }

//    @Test
//    public void Should_addNewHotelIfHotelDoesNotExist_WhenHotelIsGiven() {
//        hotelServiceImpl = setUpData();
//        City city = new City(5, "Cardiff", 51.4837, 3.1681);
//        List<Hotel> expectedHotelsList = new ArrayList<>();
//        expectedHotelsList.add(setupExpectedHotelData(1, "Premier Inn", city, 51.5075, 0.1278, 2));
//        expectedHotelsList.add(setupExpectedHotelData(2, "Arms Inn", city, 51.5064, 0.1244, 3));
//        expectedHotelsList.add(setupExpectedHotelData(3, "Edison Hotel", city, 51.5101, 0.1231, 5));
//
//        Hotel expectedNewHotel = setupExpectedHotelData(16, "The Dragon's Den", city, 51.4523, 3.1523, 3);
//        hotelServiceImpl.updateHotelList(expectedNewHotel);
//        assertTrue(expectedHotelsList.contains(expectedNewHotel));
//    }


    private HotelServiceImpl setUpData(){
        List<Hotel> hotels = new ArrayList<>();
        List<City> cities = new ArrayList<>();
        cities.add(new City(1, "London", 51.5072, 0.1276));
        cities.add(new City(2, "Leeds", 53.8008, 1.5491));
        cities.add(new City(3, "Manchester", 53.4808, 2.2426));
        cities.add(new City(4, "Edinburgh", 55.9533, 3.1883));
        cities.add(new City(5, "Cardiff", 51.4837, 3.1681));

        hotels.add(new Hotel(1, "Premier Inn", cities.get(0), 51.5075, 0.1278, 2));
        hotels.add(new Hotel(2, "Arms Inn", cities.get(0), 51.5064, 0.1244, 3));
        hotels.add(new Hotel(3, "Edison Hotel", cities.get(0), 51.5101, 0.1231, 5));
        hotels.add(new Hotel(4, "Queen's Hotel", cities.get(1), 53.8008, 1.5490, 4));
        hotels.add(new Hotel(5, "Bell Inn", cities.get(1), 53.8192, 1.5601, 3));
        hotels.add(new Hotel(6, "Travelodge", cities.get(1), 53.8052, 1.5467, 2));
        hotels.add(new Hotel(7, "Premier Inn", cities.get(2), 53.4784, 2.2432, 3));
        hotels.add(new Hotel(8, "Travelodge", cities.get(2), 53.4401, 2.2643, 1));
        hotels.add(new Hotel(9, "White's Hotel", cities.get(2), 53.4801, 2.2424, 5));
        hotels.add(new Hotel(10, "Premier Inn", cities.get(3), 55.9231, 3.1654, 2));
        hotels.add(new Hotel(11, "King's Hotel", cities.get(3), 55.9331, 3.1742, 4));
        hotels.add(new Hotel(12, "Scottish Inn", cities.get(3), 55.9621, 3.201, 3));
        hotels.add(new Hotel(13, "Premier Inn", cities.get(4), 51.4912, 3.1721, 1));
        hotels.add(new Hotel(14, "Welsh Inn", cities.get(4), 51.4512, 3.1581, 3));
        hotels.add(new Hotel(15, "Carlton Hotel", cities.get(4), 51.4836, 3.1680, 5));

        return new HotelServiceImpl(hotels);
    }
}